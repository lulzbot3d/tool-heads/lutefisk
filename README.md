LulzBot honors your freedom to print.
Add 1.75 mm filament capability to your LulzBot 3D printer and open up an even wider variety of 3D printing materials.  The M175 Tool Head is here to bring the world of 1.75 mm material to your LulzBot Mini 2, TAZ Workhorse, and TAZ Pro. 

https://www.lulzbot.com/store/tool-heads/m175-tool-head-175-mm-single-extruder-05-mm

Source and Documentation release schedule for LulzBot M175 Tool Head:
- 08/24/2020  OHAI instructions and BOM
- 09/24/2020  .stl
- 10/24/2020  step files
